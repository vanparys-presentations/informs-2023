\documentclass[9pt]{beamer}
% \documentclass[notes, handout, 9pt]{beamer}
% \documentclass[handout, 9pt]{beamer}

\usepackage{import}
\subimport{.}{preamble.tex}
\subimport{.}{beamer-theme.tex}
\usepackage{appendixnumberbeamer}
\usepackage{svg}

\usepackage[
backend=biber,
style=authoryear-icomp,
natbib=true,
url=false,
doi=false,
ibidtracker=false,
eprint=false,
dashed=false,
%issn=false,
maxbibnames=99,
maxcitenames=1
]{biblatex}
\addbibresource{references.bib}

\usepackage{fontawesome5}
\usepackage{tabularx}
\usepackage{booktabs}
\usetikzlibrary{tikzmark}
\usetikzlibrary{arrows.meta}
\newcommand{\loss}{\ell}

\makeatletter
\newcommand{\github}[1]{%
   \href{#1}{\faGithubSquare}%
}
\makeatother

% Location for all figures
\graphicspath{{pictures/}{figures/}}
% \usetikzlibrary{external}
% \tikzexternalize[prefix=figures/]

%--------------------------------------------------
% Presentation Information
%--------------------------------------------------
\title[Smooth Divergence DRO]{Smooth Divergence Distributionally Robust Optimization}
\author[B. Van Parys]{Zhenyuan Liu \and \textbf{Bart P.G.\ Van Parys} \and Henry Lam}
\date[INFORMS 2023]{}
%\institute[]{MIT Sloan School of Management}
%--------------------------------------------------

\begin{document}

\begin{frame}

  \begin{minipage}{0.35\textwidth}
    \vspace{-2mm}
    \begin{center}
      \includegraphics[height=1cm]{mit_logo.pdf}
    \end{center}
  \end{minipage}

  
  \maketitle

  \vfill
  
  \begin{center}
    \url{https://arxiv.org/abs/2306.14041}
  \end{center}

  \vfill
  
\end{frame}



\begin{frame}{Data-Driven Decisions ($DDD$)}

  \textbf{Classical} approaches for decisions in the face of uncertainty
  \[
    \min_{\theta\in \Theta} \,\E{\color{red}\P}{\ell(\theta, \xi)}
  \]
  use the \emph{distribution} ${\color{red}\P}$ of $\xi\in \Xi$ as a primitive.

  % \vfill

  % \begin{minipage}{0.4\textwidth}
  %   \begin{center}
  %     \hspace{-2em}\red{\tiny Distribution}\\
  %     \vspace{-2em}
  %     \includegraphics[height=2cm]{distribution-vs-samples/distribution.pdf}
  %   \end{center}
  % \end{minipage}%
  % \hfill
  % \begin{minipage}{0.2\textwidth}
  %   \uncover<2->{
  %     \begin{center}
  %       $\Longrightarrow$
  %     \end{center}}
  % \end{minipage}%
  % \hfill
  % \begin{minipage}{0.4\textwidth}
  %   \uncover<2->{
  %     \begin{center}
  %       \hspace{-2em}\blue{\tiny Data}\\
  %       \vspace{-2em}
  %       \includegraphics[height=2cm]{distribution-vs-samples/samples.pdf}
  %     \end{center}
  %   }
  % \end{minipage}%
  
  \vfill

  \textbf{Modern} approaches try to learn good decisions directly from \emph{data}
  \begin{equation*}
    \begin{array}{r@{}cl}
      \mathrm{Data :}~ {\xi_1, \dots, \xi_n} &% 
                                                 \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)},vertex/.style={anchor=base,
                                                     circle,fill=black!25,minimum size=18pt,inner sep=2pt}]
                                                   \pgfmathsetmacro{\cubex}{2}
                                                   \pgfmathsetmacro{\cubey}{1}
                                                   \pgfmathsetmacro{\cubez}{1}

                                                   \draw[line width=1.5pt, ->] (-\cubex-1, -\cubey/2, -\cubez/2) -- (-\cubex, -\cubey/2, -\cubez/2);
                                                   
                                                   \draw[draw=black, fill=black!60] (0,0,0) -- ++(-\cubex,0,0) -- ++(0,-\cubey,0) -- ++(\cubex,0,0) -- cycle;
                                                   \draw[draw=black, fill=black!60] (0,0,0) -- ++(0,0,-\cubez) -- ++(0,-\cubey,0) -- ++(0,0,\cubez) -- cycle;
                                                   \draw[draw=black, fill=black!60] (0,0,0) -- ++(-\cubex,0,0) -- ++(0,0,-\cubez) -- ++(\cubex,0,0) -- cycle;

                                                   \draw[line width=1.5pt, ->] (0, -\cubey/2, -\cubez/2) -- (1, -\cubey/2, -\cubez/2);

                                                   \node at (-\cubex/2, -1*\cubey/2, 0) {$\color{white} DDD$ };
                                                 \end{tikzpicture} &%
                                                                     \mathrm{Decision :} ~ {\widehat \theta_n}
    \end{array}
  \end{equation*}

  % \vfill

  % \uncover<2->{
  % Typically,
  % \[
  %   \min_{\theta\in\Theta} \widehat c(\theta, \widehat \P_n)
  % \]
  % where $\hat c$ serves as a proxy to $\E{\color{red}\P}{\ell(\theta, \xi)}$ based on \emph{empirical distribution} $\widehat \P_n$.
  % }



\end{frame}

\begin{frame}{Empirical Risk Minimization (ERM/SAA)}

  \begin{minipage}{0.35\textwidth}
    \begin{center}%
      \includegraphics[height=3cm]{statistical-error/statistical-error.pdf}      
    \end{center}
  \end{minipage}

  \vfill

  \begin{itemize} 
  \item Empirical risk minimization
    \begin{align*}
      \widehat\theta_n\in&\arg\min_{\theta\in\Theta} \E{\color{blue}\widehat \P_n}{\ell(\theta, \xi)}
    \end{align*}\\[1em]
  \item<2-> ``Optimizer's Curse'' {\tiny \citep{michaud1989markowitz}}
    \begin{align*}
      0 \ll & {\rm{Pr}}  \Big[ \underbrace{\E{\color{red}\P}{\ell(\widehat \theta_n, \xi)} > \E{\color{blue}\widehat \P_n}{\ell(\widehat \theta_n, \xi)}}_{\rm{Disappointment}} \Big] \uncover<3->{\leq {\rm{Pr}}  \Big[\underbrace{\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \E{\color{blue}\widehat \P_n}{\ell(\theta, \xi)}}_{{\rm{Uniform~Disappointment}}}\Big]} 
    \end{align*}\\[1em]
    % \item<3-> Uniform OOS disappointment probability
    %   \[
    %     {\rm{Pr}}  \Big[\underbrace{\tE{\color{red}\P}{\ell(\widehat \theta_n, \xi)} > \tE{\color{blue}\widehat \P_n}{\ell(\widehat \theta_n, \xi)}}_{{\rm{Disappointment}}} \Big]\leq {\rm{Pr}}  \Big[\underbrace{\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \E{\color{blue}\widehat \P_n}{\ell(\theta, \xi)}}_{{\rm{Uniform~Disappointment}}}\Big]
    %   \]
  \end{itemize}
  

\end{frame}

% \begin{frame}{Loss Function Complexity}


%   A naive quick fix is to consider \emph{cost inflation}, i.e.,
%   \[
%     {\rm{Pr}}  \Big[\exists \theta\in\Theta:~ \underbrace{\E{\color{red}\P}{\ell(\theta, \xi)}}_{true~cost} > \underbrace{\E{\color{blue}\widehat \P_n}{\ell(\theta, \xi)}}_{SAA~cost} + \delta \Big]\uncover<2->{\leq g(n, \mc C) \exp\left(-\frac{\delta^2 n}{\mc V}\right)}
%   \]
%   \uncover<2->{where $\mc C$ is a \emph{complexity} measure and $\mc V$ is a \emph{variation} measure of $\set{\xi \mapsto \ell(\theta,\xi)}{\theta\in\Theta}$.}

%   \vfill

%   \uncover<3->{
%     \textbf{Not desirable} as $\mc C$ and $\mc V$ may be hard to estimate.
%   }
  
  
  
% \end{frame}

\begin{frame}{Worst-case Formulations}

  \begin{minipage}[t]{0.66\textwidth}
    Worst-case formulations    
    \[
      \widehat \theta_n \in \arg\min_{\theta\in \Theta} \max_{\P'\in {\color{blue}\mc U_n(\widehat \P_n)}} \E{\P'}{\ell(\theta, \xi)}
    \]
    are often as tractable as their nominal counterpart and guard against overfitting.
  \end{minipage}%
  \hfill%
  \uncover<1->{
  \begin{minipage}[t]{0.33\textwidth}
    \vspace{0em}
    \centering
    \includegraphics[height=3cm]{dro/dro.pdf}
  \end{minipage}}
  
  \vfill

  \textbf{Geometry}:
  \begin{itemize}
  \item Moment sets {\tiny \citep{delage2010distributionally, wiesemann2014distributionally, vanparys2016generalized, }}
  \item Divergence sets {\tiny \citep{postek2016computationally, bertsimas2018data, vanparys2020data, lam2019recovering, gupta2019near}}
  \item Wasserstein balls {\tiny \citep{esfahani2018data, gao2016distributionally}}
  \end{itemize}

  \vfill

  \uncover<2->{
  \textbf{Size}: How large should {\color{blue}$\mc U_n(\widehat \P_n) = \tset{\P'}{D(\widehat \P_n, \P')\leq r}$} be to bound
  \begin{equation}
    \label{eq:uniform-oos-guarantee}
    \tag{UD}
    {\rm{Pr}}  \left[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \textstyle\sup_{\P' \in  {\color{blue} \mc U_n(\widehat \P_n)}}\E{\P'}{\ell(\theta, \xi)} \right]
  \end{equation}}

\end{frame}

\begin{frame}{${\rm{Pr}}  \big[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \textstyle\sup_{\P' \in \mc U_n(\widehat \P_n)}\E{\P'}{\ell(\theta, \xi)} \big]$ ~~ \eqref{eq:uniform-oos-guarantee}}

  \begin{minipage}[t]{0.5\textwidth}
    \vspace{0em}
    \centering
    \includegraphics[height=3cm]{dro/dro.pdf}
  \end{minipage}%
  \hfill%
  \begin{minipage}[t]{0.5\textwidth}
    \vspace{0em}
    \centering
    \only<3>{\includegraphics[height=3cm]{dro/dro2.pdf}}%
    \only<4->{\includegraphics[height=3cm]{dro/dro3.pdf}}
  \end{minipage}
 
  \vfill

  \begin{enumerate}

  \item \textbf{Cover $\P$ with high-probabilty}:

    \[
      (\ref{eq:uniform-oos-guarantee}) 
      \leq {\rm{Pr}}\left[{\color{red}\mb P} \not\in {\color{blue} \mc U_n(\widehat \P_n)} \right] \leq \alpha
    \]
    
    \begin{itemize}
    \item Wasserstein: \emph{Nonparametric rate} $r = \mc O(n^{-1/\dim(\Xi)})$ {\footnotesize\citep{dudley1969speed, fournier2015rate}}\\[0.5em]
    \item<2-> Divergence: \emph{Parametric rate} $r = \mc O(n^{-1})$. Only works if ${\color{red}\P}$ is finitely supported
    \end{itemize}
  \item<3-> \textbf{Uniform Variability Expansion}: $$\sup_{\P' \in {\color{blue}\mc U_n(\widehat \P_n)}}\E{\P'}{\ell(\theta, \xi)} = \underbrace{\E{\color{blue}\widehat \P_n}{\ell(\theta, \xi)}}_{ERM/SAA} + \underbrace{\V{\color{red}\widehat \P}{\ell(\theta, \xi)} \sqrt{r}}_{Regularization}+o(\sqrt{r})$$
    implies
    \(
      \eqref{eq:uniform-oos-guarantee} \leq \alpha
    \)
    with {parametric rate} $r = \mc O(n^{-1})$ \emph{when loss function $\ell$ is ``nice''.}
  \end{enumerate}
  
  
\end{frame}

\begin{frame}{Goal of this talk}

  \textbf{Main Question:} Does there exists a formulation that is ``statistically efficient'' and impose no restriction on the loss function $\ell$? 

\end{frame}

\begin{frame}{Finite Support {\footnotesize\citep{vanparys2020data}}}

  \begin{minipage}[t]{0.65\textwidth}
    Consider the \emph{$f$-divergence} metric
    \[
    D_f(\P', \P) = \int \! f\!\left(\frac{\d \P'}{\d \P}(\xi)\right) \d \P'(\xi)
    \]
    and its associated \emph{divergence DRO} formulation
    \[
      \inf_{\theta\in\Theta}~ \{ \widehat c_f(\theta, {\color{blue}\widehat \P_n}) \defn \textstyle \sup_{D_f({\color{blue}\widehat \P_n}, \P')\leq r}\E{\P'}{\ell(\theta, \xi)}\}.
    \]
  \end{minipage}%
  \hfill
  \begin{minipage}[t]{0.35\textwidth}
    \vspace{0em}
    \centering
    \uncover<1->{
      \begin{tikzpicture}[scale=0.6]
        % grid
        % \draw[step=0.5cm,color=gray] (-3,-3) grid (10,7);
        % grid
        % \draw[step=0.5cm,color=gray] (0.5,1) grid (6.5,5);
        % AXIS------------------
        \draw[->] (0.3,1) -- (6.5,1) node[below] {$\theta, {\widehat \P}$};
        \draw[->] (0.5,0.8) -- (0.5,5);
        % TICKS
        % \foreach \x/\xtext in {1/1, 2/2, 3/3,4/4,5/5,6/6}
        % \draw[shift={(\x,1)}] (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};
        % \foreach \y/\ytext in {2/2, 3/3,4/4,5/5}
        % \draw[shift={(0,\y)}] (2pt,0pt) -- (-2pt,0pt) node[left] {$\ytext$};

        % CURVES----------------------
        % true cost
        \draw[thick] plot [smooth,tension=0.9] coordinates{
          (0.5, 2.0)
          (2.5, 1.5)
          (5.0, 2.5)
          (6.5, 2.5)
        };
        \draw (6.7, 1.7) node[left] 
        {$\E{\widehat \P}{\ell(\theta, \xi)}$};

        % c_2
        \uncover<4->{
          \draw[thick, color=orange] plot [smooth,tension=0.9] coordinates{
            (0.5, 4)
            % (1.8, 3)
            (3,3)
            (5.5, 3.5)
            (6.5, 3.5)
          };
          \draw (1.5, 4) node[left] 
          {${\color{orange}\widehat{c}}$};}

        % feasible c_3
        \draw[thick, color=green] plot [smooth,tension=0.9] coordinates{
          (0.5, 2.5)
          (2,2)
          (5.5, 3)
          (6.5, 3)
        };
        \draw (3.1, 2.5) node[left] 
        {${\color{green}\widehat c_{\KL}}$};

        
      \end{tikzpicture}}
  \end{minipage}%


  \vfill

  \uncover<2->{
    \textbf{Out-of-Sample Guarantee:} Let $\Xi$ be finite. Then, for all ${\color{red}\P}$ we have
  \begin{equation}
    \label{eq:feasibility:1}
    \only<2>{{\rm{Pr}}  \left[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > {\color{green}\widehat c_{\KL}}(\theta,  {\color{blue}\widehat\P_n}) \right] \leq (n+1)^{\abs{\Xi}}\exp(-rn).}
    \only<3->{\limsup_{n\to\infty} \frac{1}{n} \log {\rm{Pr}}  \left[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \widehat c_{\KL}(\theta, {\color{blue}\widehat \P_n}) \right] \leq -r.}
  \end{equation}}
  
  \vfill

  \uncover<4->{
  \textbf{Least Conservative:} Let $\Xi$ be finite. Then, for any continuous $\widehat c$ feasible in \eqref{eq:feasibility:1} we have $${\color{orange}\widehat c} (\theta, \widehat\P')\geq {\color{green}\widehat c_{\KL}}(\theta, {\color{blue}\widehat \P'}) \quad \forall \theta\in\Theta, ~\forall {\color{blue}\widehat\P'}.$$}
  
  
\end{frame}

\begin{frame}{Infinite Support}

  \textbf{Theorem (Failure of KL DRO):} Let $\Theta=[0,1]$ and ${\color{red}\P}$ the uniform distribution on $\Xi=[-1, 1]$. For all $0 \leq r$ there exists a continuous loss function $\ell$ so that
  \begin{align*}
    & {\rm{Pr}}  \big[\underbrace{\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \widehat c_{\KL}(\theta, {\color{blue}\widehat \P_n})}_{\rm{Uniform~Disappointment}} \big] = 1, \quad \forall n\geq 1.\\[-1em]
    \uncover<2->{\intertext{For all $0\leq r$ there exists a continuous loss function $\ell$ so that}
    & {\rm{Pr}}  \big[\underbrace{\tE{\color{red}\P}{\ell(\widehat \theta_{KL}, \xi)} > \widehat c_{\KL}(\widehat \theta_{KL}, {\color{blue}\widehat \P_n})}_{\rm{Disappointment}} \big] = 1,% \\[0.5em]
    % & {\rm{Pr}}  \left[{\textstyle\min_{\theta\in \Theta} \E{\color{red}\P}{\ell(\theta, \xi)} > \min_{\theta\in\Theta} \widehat c_{\KL}(\theta, \widehat \P_n)} \right]=1.
}
  \end{align*}
  
  \vfill

  \uncover<3->{
  \textbf{Proof Sketch:} Consider ``uniformly distributed modulo 1'' loss functions from analytic number theory.}
  
\end{frame}

\begin{frame}{Discontinuous Divergences}

  \textbf{Observation:} KL-divergence lacks (weak) continuity

  \begin{center}
    \begin{tikzpicture}[scale=0.9]
      \begin{axis}[
        axis lines=center,
        y=1cm,            % y unit vector
        x=10cm,            % x unit vector
        xmin = 0.2,
        xmax = 0.9,
        ymin = 0,
        ymax = 3,
        clip=true,
        ytick=\empty,
        xtick=\empty
        ]

        \node[circle, draw=blue, inner sep=1, fill=blue] (p1) at (axis cs:0.3, 1) {};
        \node[circle, draw=blue, inner sep=1, fill=blue] (p2) at (axis cs:0.8, 1) {};    
        \draw[blue, thick] (p1) -- (p2);
        \draw[blue, densely dotted, thick] (axis cs:0.3, 0) -- (p1);
        \draw[blue, densely dotted, thick] (axis cs:0.8, 0) -- (p2);

        \only<2->{
          \draw[blue, densely dotted, thick] (axis cs:0.8, 0) -- (axis cs:0.8, 1.5);
          \node[circle, draw=red, inner sep=1, fill=red] (p1) at (axis cs:0.32, 1) {};
          \node[circle, draw=red, inner sep=1, fill=red] (p2) at (axis cs:0.82, 1) {};    
          \draw[red, thick] (p1) -- (p2);
          \draw[red, densely dotted, thick] (axis cs:0.32, 0) -- (p1);
          \draw[red, densely dotted, thick] (axis cs:0.82, 0) -- (axis cs:0.82, 1.5);
          \node[] at (axis cs:0.85, 0.5) {{\color{red}$\nu$}};

          \draw[<->, black] (axis cs:0.8, 1.5) -- (axis cs:0.82, 1.5);

          \node at (axis cs:0.81, 1.75) {$\small\epsilon>0$};
        }
        
        

        \node[] at (axis cs:0.25, 0.5) {{\color{blue}$\mu$}};
       

        \only<1>{
          \node[right] at (axis cs:0.4, 2) {{$\KL({\color{blue}\mu}, {\color{blue}\mu})=0$}};}
        \only<2->{
          \node[right] at (axis cs:0.4, 2) {{$\KL({\color{blue}\mu}, {\color{red}\nu})=\infty$}};}
        
        
      \end{axis}      
    \end{tikzpicture}
  \end{center}

  \uncover<2->{
  \textbf{Remarks:}
  \begin{itemize}
  \item Lack of continuity directly results in lack of coverage
  \item<3-> L\'evy-Prokhorov (LP) and Wasserstein (W) distances do not suffer this shortcoming.
  \item<4-> Denote with $\S=\{\LP, \W\}$ a weakly continuous ``smooth'' distance
  \end{itemize}}
  
\end{frame}


\begin{frame}{Smooth Divergences}

  
  \begin{minipage}[t]{0.5\textwidth}
    \vspace{0em}
    \centering
    \only<1>{\includegraphics[height=4cm]{smooth-dro/nonsmooth-dro.pdf}}
    \only<2->{\includegraphics[height=4cm]{smooth-dro/smooth-dro.pdf}}
  \end{minipage}%
  \vfill
  
  
  \textbf{Smooth Divergences}
  \[
    \uncover<2->{D^\epsilon_{f, \S}({\color{blue}\P'}, {\color{red}\P}) = \inf \{}D_f(\only<1>{{\color{blue}\P'}}\only<2->{{\color{green}\mb Q}}, {\P}) \uncover<2->{: \exists {\color{green}\mb Q}\in \mc P,~ {\color{green}\S}({\P'}, {\color{green}\mb Q})\leq \epsilon \}} \uncover<3->{\leq D_{f}({\color{blue}\P'}, {\color{red}\P})}
  \]
  
  \vfill
  
\end{frame}


\begin{frame}{Efficiency}

  \begin{minipage}[t]{0.65\textwidth}
    \emph{Smooth divergence DRO} formulation
    \[
      \inf_{\theta\in\Theta}~ \{ \widehat c^\epsilon_{f, \S}(\theta, {\color{blue}\widehat \P_n}) \defn \textstyle \sup_{D^\epsilon_{f,\S}({\color{blue}\widehat \P_n}, \P')\leq r}\E{\P'}{\ell(\theta, \xi)}\}.
    \]
  \end{minipage}%
  \hfill
  \begin{minipage}[t]{0.35\textwidth}
    \vspace{0em}
    \centering
    \uncover<1->{
      \begin{tikzpicture}[scale=0.6]
        % grid
        % \draw[step=0.5cm,color=gray] (-3,-3) grid (10,7);
        % grid
        % \draw[step=0.5cm,color=gray] (0.5,1) grid (6.5,5);
        % AXIS------------------
        \draw[->] (0.3,1) -- (6.5,1) node[below] {$\theta, {\widehat \P}$};
        \draw[->] (0.5,0.8) -- (0.5,5);
        % TICKS
        % \foreach \x/\xtext in {1/1, 2/2, 3/3,4/4,5/5,6/6}
        % \draw[shift={(\x,1)}] (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};
        % \foreach \y/\ytext in {2/2, 3/3,4/4,5/5}
        % \draw[shift={(0,\y)}] (2pt,0pt) -- (-2pt,0pt) node[left] {$\ytext$};

        % CURVES----------------------
        % true cost
        \draw[thick] plot [smooth,tension=0.9] coordinates{
          (0.5, 2.0)
          (2.5, 1.5)
          (5.0, 2.5)
          (6.5, 2.5)
        };
        \draw (6.7, 1.7) node[left] 
        {$\E{\widehat \P}{\ell(\theta, \xi)}$};

        % c_2
        \uncover<3->{
          \draw[thick, color=orange] plot [smooth,tension=0.9] coordinates{
            (0.5, 4)
            % (1.8, 3)
            (3,3)
            (5.5, 3.5)
            (6.5, 3.5)
          };
          \draw (1.5, 4) node[left] 
          {${\color{orange}\widehat{c}}$};}

        \uncover<3->{
        % feasible c_3
        \draw[thick, color=green!50] plot [smooth,tension=0.9] coordinates{
          (0.5, 2.5)
          (2,2.1)
          (5.5, 3.2)
          (6.5, 3)
        };
        \draw (3, 2.25) node[below left] 
        {${\color{green!50}\widehat c_{\KL}}$};}

        % feasible c_4
        \draw[thick, color=green] plot [smooth,tension=0.9] coordinates{
          (0.5, 2.6)
          (2,2.2)
          (5.5, 3.3)
          (6.5, 3.1)
        };
        \draw (2, 2.7) node[left] 
        {${\color{green}\widehat c^\epsilon_{\KL, \S}}$};

        
      \end{tikzpicture}}
  \end{minipage}%

  \vfill
  
  \uncover<1->{
    \textbf{Out-of-Sample Guarantee:} Let $\Xi$ be compact. Then, for all ${\color{red}\P}$, $r\geq 0$ and $\epsilon>0$ we have
    \begin{equation}
      \label{eq:feasibility}
      \only<1>{{\rm{Pr}}  \left[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \widehat c^\epsilon_{\KL, S}(\theta, {\color{blue}\widehat \P_n}) \right] \leq (8/\epsilon)^{\mc C(\Xi, \epsilon/2)} \exp(-rn)}
      \only<2->{\limsup_{n\to\infty} {\textstyle\frac{1}{n}} \log {\rm{Pr}}  \left[\exists \theta\in\Theta:~ \E{\color{red}\P}{\ell(\theta, \xi)} > \widehat c^\epsilon_{\KL, S}(\theta, {\color{blue}\widehat \P_n}) \right] \leq -r.}
    \end{equation}}
  with $\mc C(\Xi, \epsilon)$ the covering number of $\Xi$ with $\epsilon$-norm balls.
  
  \vfill

  \uncover<3->{
    \textbf{Least Conservative:} Let $\Xi$ be compact and $\xi \mapsto \ell(\theta, \xi)$ uppersemicontinuous for all $\theta\in\Theta$. Then, for any continuous $\widehat c$ feasible in \eqref{eq:feasibility} we have $$ \widehat c (\theta, {\color{blue}\widehat\P'})\geq c_{\KL}(\theta, \widehat \P_n)= \lim_{\epsilon\to 0} \widehat c^\epsilon_{\KL, S}(\theta, {\color{blue}\widehat \P'}) \quad \forall \theta\in\Theta, ~\forall {\color{blue}\widehat\P'}.$$}

  
\end{frame}

\begin{frame}{Computation}
  
  \textbf{Observation:} We assume that
  \[
    \S(P', P) = \textstyle \inf_{T\in \Gamma(P', P)} d(\xi', \xi) \, \d T(\xi', \xi)
  \]
  is an optimal transport distance with LSC transport cost $d:\Xi\times\Xi\to\Re_+$, e.g.,
  \begin{itemize}
  \item {\color{gray}$1$-Wasserstein : $d(\xi', \xi)=\norm{\xi'-\xi}$ {\footnotesize\citep{esfahani2018data}}}
  \item {\color{gray}L\'evy-Prokhorov : $d(\xi', \xi)=\one{\norm{\xi'-\xi}\geq \epsilon}$ {\footnotesize \citep{huber1964robust}}}
  \end{itemize}
  
  \vfill

  
  \uncover<2->{
  \textbf{Proposition:} Smooth Divergence DRO reduces to
  \[
    \begin{array}{r@{~}l}
     \widehat c_{f, d}^\epsilon(\theta, {\color{blue} \widehat \P_n}) = \min &\int  [\max_{\delta\geq 0} \lambda f^\star(\tfrac{(\ell^\delta(\theta, \xi)-\eta)}{\lambda}) - \gamma \delta ] \, \d {\color{blue} \widehat \P_n}(\xi) +  \epsilon\gamma + r \lambda+ \eta\\[0.5em]
      \st &\gamma\geq 0,~\eta\in\Re, ~\lambda\in\Re_+, \\[0.5em]
           & \ell^{\infty}(\theta)-\eta\leq f^\infty \lambda
    \end{array}
  \]
  \begin{itemize}
  \item<3-> with $\ell^\delta(\theta, \xi) = \max_{\xi'\in\Xi, \, d(\xi, \xi')\leq \delta} \ell(\theta, \xi')$ the \emph{$\delta$-inflated loss function},
  \item<4-> $f^\star$ the \emph{convex conjugate} of $f$, and $f^\infty=\lim_{u\to\infty}f(u)/u$.
  \end{itemize}
  }
\end{frame}

\begin{frame}{Related Ideas}

  \begin{itemize}
    \setlength\itemsep{2em}
  \item \citet{dupuis2022formulation} study regularized divergence distances
    \[
      D^\gamma_{f, W}(\P', \P) = \inf_{\mb Q}  W(\P', \mb Q)/\gamma  +  D_f(\mb Q, \P).
    \]
    Special case of our smooth divergence distances.
  \item<2-> We have $\tE{}{D_W({\color{blue}\widehat \P_n}, {\color{red}\P})} = \Omega(n^{-1/\dim(\Xi)})$ {\footnotesize\citep{dudley1969speed}}.
    \citet{goldfeld2020convergence} study regularized Wasserstein distances
    \[
      D_W^{\sigma^2}(\P', \P) = W(N(0, \sigma^2) \otimes \P', N(0, \sigma^2) \otimes  \mb P)
    \]
    and show that under mild conditions on $\P$ for any $\sigma>0$ we can obtain
    \[
      \E{}{D_W^{\sigma^2}({\color{blue}\widehat \P_n}, {\color{red}\P})} = O(n^{-1/2}).
    \]
  \end{itemize}
  
\end{frame}

\begin{frame}{Conclusions}


  \begin{itemize}
    \setlength\itemsep{2em}
  \item Wasserstein DRO provides coverage but is statistically conservative.
  \item Divergence DRO is statistically efficient only when $\Xi$ is finite.
  \item<2-> Smooth Divergence DRO formulation through Wasserstein smoothing provides \emph{coverage with guaranteed statistical efficiency}
  \end{itemize}

  \vfill

  \uncover<2->{
  \begin{center}
    \url{https://arxiv.org/abs/2306.14041}
  \end{center}}


  
\end{frame}

\appendix

\begin{frame}[allowframebreaks]{References}
  \printbibliography
\end{frame}

\section{L\'evy-Prokhorov Distance}

\begin{frame}{L\'evy-Prokhorov Distance}

  
  \begin{minipage}{0.6\textwidth}
    The L\'evy-Prokhorov distance is characterized through its sublevel sets
  \begin{align*}
    &\set{\P'}{\LP(\widehat \P, \P')\leq {\color{green}\epsilon}}\\
      = & \set{\P'}{\P'({\color{blue}S})\leq {\widehat \P}({\color{red}S^{\color{green}\epsilon}})+{\color{green}\epsilon} ~~ \forall {\color{blue}S}}
  \end{align*}
  where ${\color{red}S}^{\color{green}\epsilon}\defn \set{\xi'\in \Xi}{\exists \xi\in {\color{blue}S}, ~\norm{\xi'-\xi}\leq {\color{green}\epsilon}}$.

  \vspace{1em}
  
  \textbf{Properties:}
    \begin{itemize}
    \item Symmetric and bounded by one {\tiny\citep{huber1964robust}}
    \item Metrizes the weak topology on $\mc P$
    \end{itemize}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.4\textwidth}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/eps-inflation/eps-inflation.pdf}
  \end{minipage}
  \vfill

  
\end{frame}

\begin{frame}{Strassen's Theorem}

  \begin{minipage}[t]{0.4\textwidth}
    \vspace{0em}
    \centering
    \includegraphics[height=3.8cm]{figures/levy-prokhorov/lp.pdf}
    \begin{tikzpicture}[overlay]
      \node at (-3.5,3.55) {{\color{blue} $\widehat \P$}};
      \node at (-3.5,2.5) {{\color{orange} $T$}};
      \node[rotate=-90] at (-0.4,2.85) {{\color{green} $ {\mathbb P}'$}};
      \draw[{Stealth}-{Stealth}, draw=black] (-1.4,3.175) -- (-1.4, 1.87) node [pos=0.4, sloped, above] {${\tiny 2}\epsilon$} ;
    \end{tikzpicture}
  \end{minipage}

  \vfill
  
  \textbf{Theorem} \citep{strassen1965existence}: The L\'evy-Prokhorov distance satisfies
  \begin{align*}
    & \set{\P'}{\LP(\widehat\P, \P')\leq {\color{green}\epsilon}}
      = \set{\P'}{\exists T \in \Gamma(\widehat\P, \P'),~\textstyle\int \one{\norm{\xi-\xi'}\leq {\color{green}\epsilon}} \d T(\xi, \xi')\geq 1-{\color{green}\epsilon}}.
  \end{align*}

  
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

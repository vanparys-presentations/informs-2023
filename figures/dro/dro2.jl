using Contour
using CSV
using DataFrames
using LinearAlgebra
using Convex
using ECOS

"""
    relative_entropy(P, Q)

    Relative entropy between two distributions P and Q.
"""
function relative_entropy(P, Q)
    d = length(P)
    I = NaN

    if(minimum(P)>=0 && minimum(Q)>0 && sum(P)==1 && sum(Q)==1)
        I = 0
        for i = 1:d
            if (P[i]>0 && Q[i]>0)
                I = I + P[i]*log(P[i]/Q[i])
            elseif (P[i]>0 && Q[i]==0) 
                I = NaN
            end
        end
    end

    return I
end

function wc_cost(P_ref, γ, r)
    Q = Variable(length(P_ref))

    constrs = [Q>=0]+ [sum(Q)==1]
    constrs = constrs + [sum([Convex.relative_entropy(Convex.Constant(P_ref[i]), Q[i]) for i in 1:length(P_ref)])<=r]
    problem=maximize(dot(γ, Q), constrs)

    solve!(problem, ECOS.Optimizer)

    return problem.optval
end

# Grid the probability simplex
Δ = 0.001
P_ref = [1.5/8, 2/8, 4.5/8]
P = [0.2, 0.44, 0.36]
γ = P-P_ref



# Compute function
F = [relative_entropy(P_ref, [p1, p2, 1-p1-p2]) for p1 in 0:Δ:1, p2 in 0:Δ:1] 
G = [dot(γ, [p1, p2, 1-p1-p2]) for p1 in 0:Δ:1, p2 in 0:Δ:1] 

rs = [0.05, 0.06, 0.07, 0.2]

# Compute contour lines
c = contours(0:Δ:1, 0:Δ:1, F, rs)

# Save contour lines
name=[]
for cl in Contour.levels(c)
    open("contour_"*string(level(cl))*".tex", "w") do f
    
        for j in 1:length(Contour.lines(cl))
            
            line = Contour.lines(cl)[j]
            p1, p2 = coordinates(line)
            C=DataFrame(p1=p1, p2=p2, p3=1 .- p1 .- p2)
            C=C[findall(s->!isnan(s), C[!, :p3]), :]

            if size(C, 1)>1
               CSV.write("csv/contour_"*string(level(cl))*"_"*string(j)*".csv", C)
               write(f, "\\addplot3  [draw=black, densely dotted] table [x=p1,y=p2, z=p3, col sep=comma] {csv/contour_"*string(level(cl))*"_"*string(j)*".csv}; \n")
            end            
        end
    end
end

gs = [wc_cost(P_ref, γ, r) for r in rs]

# Compute contour lines
c = contours(0:Δ:1, 0:Δ:1, G, gs)

# Save contour lines
name=[]
for cl in Contour.levels(c)
    open("contour2_"*string(level(cl))*".tex", "w") do f
    
        for j in 1:length(Contour.lines(cl))
            
            line = Contour.lines(cl)[j]
            p1, p2 = coordinates(line)
            C=DataFrame(p1=p1, p2=p2, p3=1 .- p1 .- p2)
            C=C[findall(s->!isnan(s), C[!, :p3]), :]

            if size(C, 1)>1
               CSV.write("csv/contour2_"*string(level(cl))*"_"*string(j)*".csv", C)
               write(f, "\\addplot3  [draw=black, densely dotted] table [x=p1,y=p2, z=p3, col sep=comma] {csv/contour2_"*string(level(cl))*"_"*string(j)*".csv}; \n")
            end            
        end
    end
end

gs2 = [dot(γ, P) for r in rs]

# Compute contour lines
c = contours(0:Δ:1, 0:Δ:1, G, gs2)

# Save contour lines
name=[]
for cl in Contour.levels(c)
    open("contour3_"*string(level(cl))*".tex", "w") do f
    
        for j in 1:length(Contour.lines(cl))
            
            line = Contour.lines(cl)[j]
            p1, p2 = coordinates(line)
            C=DataFrame(p1=p1, p2=p2, p3=1 .- p1 .- p2)
            C=C[findall(s->!isnan(s), C[!, :p3]), :]

            if size(C, 1)>1
               CSV.write("csv/contour3_"*string(level(cl))*"_"*string(j)*".csv", C)
               write(f, "\\addplot3  [draw=black, densely dotted] table [x=p1,y=p2, z=p3, col sep=comma] {csv/contour3_"*string(level(cl))*"_"*string(j)*".csv}; \n")
            end            
        end
    end
end

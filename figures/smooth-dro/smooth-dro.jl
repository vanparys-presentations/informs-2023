using Contour
using CSV
using DataFrames
using Convex
using ECOS

function save_contour(C_c, name)
    # Save contour lines
    for cl in Contour.levels(C_c)
        open(name*".tex", "w") do f
            for j in 1:length(Contour.lines(cl))
                line = Contour.lines(cl)[j]
                p1, p2 = coordinates(line)
                C = DataFrame(p1=p1, p2=p2, p3=1 .- p1 .- p2)
                C = C[findall(s -> !isnan(s), C[!, :p3]), :]
                if size(C, 1) > 1
                    CSV.write("csv/"*name*"_" * string(j) * ".csv", C; quotechar=''')
                    write(f, "\\addplot3 [draw=blue, fill=blue!20] table [x=p1,y=p2, z=p3, col sep=comma] {csv/"*name*"_" * string(j) * ".csv}; \n")
                end
            end
        end
    end
end

"""
    relative_entropy(P, Q)

    Relative entropy between two distributions P and Q.
"""
function relative_entropy(P, Q)
    d = length(P)
    I = NaN

    if(minimum(P)>=0 && minimum(Q)>0)
        I = 0
        for i = 1:d
            if (P[i]>0 && Q[i]>0)
                I = I + P[i]*log(P[i]/Q[i])
            elseif (P[i]>0 && Q[i]==0) 
                I = NaN
            end
        end
    end

    return I
end

P = [1.5/8,2/8,4.5/8]
Pn = [0.40, 0.31, 0.29]
ϵ = 0.05

# Compute Q
Q = Variable(length(P))
constrs = [Q>=0, sum(Q)==1]
constrs = constrs + [norm(Pn-Q, 1)/2<=ϵ]
obj = sum([Convex.relative_entropy(Q[i], Convex.Constant(P[i])) for i in 1:length(P)])

problem=minimize(obj, constrs)
solve!(problem, ECOS.Optimizer)

Q = Q.value
Q = Q/sum(Q)

# Grid the probability simplex
Δ = 0.001

# Compute S distance
S = [norm(Pn- [q1, q2, 1-q1-q2], 1)/2 for q1 in 0:Δ:1, q2 in 0:Δ:1] 
# Compute S contour lines
S_c = contours(0:Δ:1, 0:Δ:1, S, [ϵ])

# Compute D distance
D = [relative_entropy([p1, p2, 1-p1-p2], P) for p1 in 0:Δ:1, p2 in 0:Δ:1] 
# Compute D contour lines
D_c1 = contours(0:Δ:1, 0:Δ:1, D, [relative_entropy(Q, P)])
D_c2 = contours(0:Δ:1, 0:Δ:1, D, [relative_entropy(Pn, P)])


save_contour(S_c, "S")
save_contour(D_c1, "D1")
save_contour(D_c2, "D2")

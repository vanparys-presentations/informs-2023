#
# Make all the tikz figures
#

SUBDIRS := figures

all: main.pdf

main.pdf: $(SUBDIRS)
	pdflatex --shell-escape main.tex
	biber main
	pdflatex --shell-escape main.tex
	pdflatex --shell-escape main.tex

clean: $(SUBDIRS)
	rm -rf *.aux *.csv *.log *.pdf *.svg *.out *.nav *.toc *.rip *.vrb *.snm *.xml *.bbl *.bcf *.blg

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(SUBDIRS)
